import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigation from './MainNavigation';
import LoginScreen from '../screens/LoginScreen';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';

const Stack = createStackNavigator();

function AppStack(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {props.statusLogin ? (
          <Stack.Screen
            name="Main"
            component={MainNavigation}
            options={{
              headerShown: false,
            }}
          />
        ) : (
          <Stack.Screen name="Login" component={LoginScreen} />
        )}
        {/* <Stack.Screen
            name="Main"
            component={MainNavigation}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen name="Login" component={LoginScreen} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  statusLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);

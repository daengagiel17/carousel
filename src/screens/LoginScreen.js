import React, {useState} from 'react';
import {Text, View, Button, TextInput} from 'react-native';
import {connect} from 'react-redux';

function LoginScreen(props) {
  const [username, setUsername] = useState(null);
  const [password, setPassword] = useState(null);

  return (
    <View>
      <Text>LOGIN YOK</Text>

      <TextInput
        onChangeText={(text) => setUsername(text)}
        value={username}
        placeholder="Usename"
        style={{backgroundColor: '#fff'}}
      />

      <TextInput
        onChangeText={(text) => setPassword(text)}
        value={password}
        placeholder="Password"
        style={{backgroundColor: '#fff'}}
        secureTextEntry={true}
      />

      <Button
        color="#444"
        onPress={() => props.processLogin(username)}
        title="GO TO MAIN"
      />
    </View>
  );
}

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => ({
  processLogin: (textUsername) =>
    dispatch({type: 'LOGIN', username: textUsername}),
});

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

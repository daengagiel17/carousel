import {combineReducers} from 'redux';
import todoReducers from './todo';
import auth from './auth';

export default combineReducers({
  todoReducers,
  auth,
});

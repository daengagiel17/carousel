const initialState = [];

import {FETCH_SUCCEEDED, FETCH_FAILED} from '../action/todo';

const todoReducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_SUCCEEDED:
      return action.receivedTodos;
    case FETCH_FAILED:
      return [];
    default:
      return state; //state does not change
  }
};

export default todoReducers;

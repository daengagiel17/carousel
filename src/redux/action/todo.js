// export const listTodoAct = (data) => ({
//   type: 'LIST TODO',
//   data,
// });

// export const addTodoAct = (text) => ({
//   type: 'ADD TODO',
//   text,
// });

// export const removeTodoAct = (text) => ({
//   type: 'REMOVE TODO',
//   text,
// });

// export const doneTodoAct = (id) => ({
//   type: 'DONE TODO',
//   id: id,
// });

export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const FETCH_TODOS = 'FETCH_TODOS';

export const FETCH_SUCCEEDED = 'FETCH_SUCCEEDED';
export const FETCH_FAILED = 'FETCH_FAILED';

export const fetchTodosAction = (sort) => {
  console.log('sort', sort);
  return {
    type: FETCH_TODOS,
    sort,
  };
};

export const addTodoAction = (text) => {
  console.info('addTodoAction()', text);
  return {
    type: ADD_TODO,
    newTodo: {
      text: text,
      completed: false,
    },
  };
};

export const updateTodoAction = (todo) => {
  console.log('todo', todo);
  return {
    type: UPDATE_TODO,
    todo,
  };
};

export const deleteTodoAction = (todo) => {
  console.log('todo', todo);
  return {
    type: DELETE_TODO,
    todo,
  };
};

//Action sent by Redux-saga
export const fetchSuccessAction = (receivedTodos) => {
  console.log('succes fetch', receivedTodos);
  return {
    type: FETCH_SUCCEEDED,
    receivedTodos,
  };
};

export const fetchFailedAction = (error) => {
  return {
    type: FETCH_FAILED,
    error,
  };
};
